package com.occastion.hashtechs.occastion.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.occastion.hashtechs.occastion.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
